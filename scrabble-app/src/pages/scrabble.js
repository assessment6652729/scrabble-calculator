import React, { useState, useEffect } from "react";
import { createTheme, ThemeProvider } from "@mui/material/styles";
import {
  Box,
  Typography,
  Button,
  Grid,
  Alert,
  Backdrop,
  CircularProgress,
  Fade,
} from "@mui/material";
import Tiles from "../components/text-box";
import BasicModal from "../components/dialog";
import {
  getTotalTiles,
  calculateScore,
  concatLetter,
} from "../services/calculator.service";
import { post, get } from "../services/client.service";

const theme = createTheme();

export default function Scrabble() {
  const [formValues, setFormValues] = useState(null);
  const [score, setScore] = useState(0);
  const [letters, setLetters] = useState([]);
  const [word, setWord] = useState();
  const [failureMessage, setFailureMessage] = useState("");
  const [showFailureMessage, setShowFailureMessage] = useState(false);
  const [successMessage, setSuccessMessage] = useState("");
  const [showSuccessMessage, setShowSuccessMessage] = useState(false);
  const [showProgressBar, setShowProgressBar] = useState(false);
  const [error, setError] = useState(false);

  const [openModal, setOpenModal] = useState(false);
  const [topScores, setTopScores] = useState([]);
  const numberOfTiles = getTotalTiles();
  const username = sessionStorage.getItem("username");

  //Calculate Score on Change
  const handleInputChange = (e) => {
    const { name, value } = e.target;
    setFormValues({ ...formValues, [name]: value }); //Updating for text fields
    setLetters(concatLetter(name, value, letters)); //To keep track of the letters
    const { totalScore, word } = calculateScore(letters);
    setWord(word);
    setScore(totalScore);
  };

  //Save Score
  const handleSaveScore = () => {
    setShowProgressBar(true);
    console.log("Saving Score ", score);
    const request = {
      score,
      word,
      username,
    };
    post("/api/scores", request, onSuccessSave, onError);
  };

  //Callback after saving score
  const onSuccessSave = (res) => {
    console.log("res.statusCode ", res.statusCode);
    setShowProgressBar(false);
    if (res.statusCode === 200) {
      handleResetTiles();
      setShowFailureMessage(false);
      setFailureMessage("");
      setShowSuccessMessage(true);
      setSuccessMessage("SAVED SCORE!");
    } else {
      setShowFailureMessage(true);
      setFailureMessage("ERROR!");
    }
  };

  //Error Callback
  const onError = (err) => {
    setOpenModal(false);
    setShowProgressBar(false);
    setFailureMessage("ERROR!");
    setShowFailureMessage(true);
    setError(err);
  };

  //Clear tiles
  const handleResetTiles = () => {
    setLetters([]);
    setWord("");
    setScore(0);
    setFormValues(null);
  };

  //View Top Scores for user
  const handleViewTopScores = () => {
    get("/api/scores?count=10", onSuccessView, onError);
  };

  const onSuccessView = (res) => {
    console.log("onSuccessView", res);
    setTopScores(res);
    setOpenModal(true);
  };

  const handleClose = () => setOpenModal(false);

  useEffect(() => {}, [letters, topScores]);

  return (
    <ThemeProvider theme={theme}>
      <BasicModal
        id="score-modal"
        openModal={openModal}
        data={topScores}
        handleClose={handleClose}
      />
      <Grid container spacing={2}>
        <Grid item xs={12}>
          <Box
            sx={{
              marginTop: 8,
              display: "flex",
              flexDirection: "column",
              alignItems: "center",
              textAlign: "center",
            }}
          >
            <Tiles
              numberOfTiles={numberOfTiles}
              onChange={handleInputChange}
              letters={letters}
            />
          </Box>
        </Grid>
        <Grid item xs={12}>
          <Box
            sx={{
              marginTop: 8,
              display: "flex",
              flexDirection: "column",
              alignItems: "center",
            }}
          >
            <Typography variant="h5">
              Score : <span id="total-score">{score}</span>
            </Typography>
          </Box>
        </Grid>
        <Grid item xs={3}></Grid>
        <Grid item xs={6}>
          <Box sx={{ textAlign: "center" }}>
            <Button
              id="save-score-btn"
              variant="contained"
              style={{ margin: "1rem" }}
              onClick={handleSaveScore}
              disabled={score === 0 ? true : false}
            >
              Save Score
            </Button>
            <Button
              id="reset-tiles-btn"
              variant="contained"
              style={{ margin: "1rem" }}
              onClick={handleResetTiles}
              disabled={score === 0 ? true : false}
            >
              Reset Tiles
            </Button>
            <Button
              id="view-scores-btn"
              variant="contained"
              style={{ margin: "1rem" }}
              onClick={handleViewTopScores}
            >
              View Top Scores
            </Button>
          </Box>
        </Grid>
        <Grid item xs={3}></Grid>
        <Grid item xs={4}></Grid>
        <Grid item xs={4}>
          <Box sx={{ textAlign: "center" }}>
            {showFailureMessage ? (
              <Alert
                onClose={() => {
                  setShowFailureMessage(false);
                }}
                severity="error"
                id="error-alert"
              >
                {" "}
                {failureMessage}{" "}
              </Alert>
            ) : (
              <></>
            )}
            {showSuccessMessage ? (
              <Fade
                in={showSuccessMessage}
                timeout={{ enter: 1000, exit: 1000 }}
                addEndListener={() => {
                  setTimeout(() => {
                    setShowSuccessMessage(false);
                  }, 2000);
                }}
              >
                <Alert
                  severity="success"
                  variant="standard"
                  className="alert"
                  id="sucess-alert"
                >
                  {successMessage}
                </Alert>
              </Fade>
            ) : (
              <></>
            )}
          </Box>
        </Grid>
        <Grid item xs={4}></Grid>
        {showProgressBar ? (
          <center>
            <div>
              {
                <center>
                  <Backdrop
                    sx={{
                      color: "#fff",
                      zIndex: (theme) => theme.zIndex.drawer + 1,
                    }}
                    open={showProgressBar}
                  >
                    <CircularProgress></CircularProgress>
                  </Backdrop>
                </center>
              }
            </div>
          </center>
        ) : (
          <></>
        )}
      </Grid>
    </ThemeProvider>
  );
}

import config from "../config.json";

function _getBaseUrl() {
  return config.baseUrl;
}

function post(uri, body, callback, errorCallback) {
  console.log("POST", uri, body);
  fetch(_getBaseUrl() + uri, {
    method: "POST",
    headers: {
      "Content-type": "application/json;",
    },
    body: JSON.stringify(body),
  })
    .then((response) => {
      return Promise.all([response.json(), response.status]);
    })
    .then(
      ([result, statusCode]) => {
        console.log("Result ", result, statusCode);
        result.statusCode = statusCode;
        callback(result);
      },
      (error) => {
        console.log("Post error ", error);
        errorCallback(error);
      }
    );
  return {};
}

function get(uri, callback, errorCallback) {
  console.log("GET");
  fetch(_getBaseUrl() + uri, {
    method: "GET",
    headers: {
      "Content-type": "application/json; charset=UTF-8",
    },
  })
    .then((response) => {
      return Promise.all([response.json(), response.status]);
    })
    .then(
      ([result, statusCode]) => {
        console.log("Result ", result, statusCode);
        callback(result);
      },
      (error) => {
        console.log("Get error ", error);
        errorCallback(error);
      }
    );
  return {};
}

export { get, post };

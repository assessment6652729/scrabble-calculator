import config from "../config.json";

function getTotalTiles() {
  return config.totalTiles;
}

function getScoreSet() {
  const scores = {
    0: [" "],
    1: ["E", "A", "I", "O", "N", "R", "T", "L", "S", "U"],
    2: ["D", "G"],
    3: ["B", "C", "M", "P"],
    4: ["F", "H", "V", "W", "Y"],
    5: ["K"],
    8: ["J", "X"],
    10: ["Q", "Z"],
  };
  return scores;
}

function getScore(input) {
  const scoreSet = getScoreSet();

  const score = Number(
    Object.keys(scoreSet).find((key) =>
      scoreSet[key].includes(input.toUpperCase())
    )
  );
  console.log("Score for ", input, score);
  return score;
}

function concatLetter(name, letter, letterArray) {
  let fieldSplit = name.split("-"); //To get index of tiles
  letterArray[fieldSplit[1]] = letter.toUpperCase();
  return letterArray;
}

function calculateScore(letterArray) {
  let totalScore = 0;
  let word = "";
  const re = /^[A-Za-z]+$/;
  letterArray.forEach((letter) => {
    if (re.test(letter)) {
      totalScore += getScore(letter);
      word += letter;
    }
  });
  console.log("Input Word ", word);
  console.log("Total Score ", totalScore);
  return { totalScore, word };
}

export { getTotalTiles, getScore, concatLetter, calculateScore };

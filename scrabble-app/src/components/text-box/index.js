import React, { useState, useEffect, createRef } from "react";
import Container from "@mui/material/Container";

export default function Tiles({ numberOfTiles, onChange, letters }) {
  const [letterArray, setLetterArray] = useState(letters);

  //Create Refs Array
  const [inputRefsArray] = useState(() =>
    Array.from({ length: numberOfTiles }, () => createRef())
  );

  const onHandleKeyUp = (index) => {
    if (letters.length <= index + 1)
      inputRefsArray[index + 1]?.current?.focus();
  };

  useEffect(() => {
    setLetterArray(letters);

    /** For cursor focus */
    if (inputRefsArray?.[0]?.current) {
      inputRefsArray?.[0]?.current?.focus();
    }
  }, [letters, inputRefsArray]);

  return (
    <Container>
      {inputRefsArray.map((ref, index) => {
        return (
          <input
            type="text"
            className="tile-box"
            ref={ref}
            key={"tile-" + index}
            id={"tile-" + index}
            name={"tile-" + index}
            value={letterArray[index] ? letterArray[index] : ""}
            maxLength="1"
            data-test={"scrabble-tile"}
            onChange={onChange}
            onKeyUp={() => onHandleKeyUp(index)}
          />
        );
      })}
    </Container>
  );
}

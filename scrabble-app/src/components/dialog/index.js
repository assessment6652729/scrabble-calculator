import React, { useState, useEffect } from "react";
import {
  Box,
  Modal,
  Table,
  TableBody,
  TableCell,
  TableHead,
  TableRow,
  Paper,
  Typography,
} from "@mui/material";

export default function BasicModal({ openModal, data, handleClose }) {
  const [open, setOpen] = useState(openModal);
  const [scores, setScores] = useState(data);

  useEffect(() => {
    setScores(data);
    setOpen(openModal);
  }, [data, openModal]);

  return (
    <div>
      <Modal
        open={open}
        onClose={handleClose}
        aria-labelledby="modal-modal-title"
        aria-describedby="modal-modal-description"
      >
        <Box sx={style}>
          <Typography variant="h5" style={{ marginBottom: "0.3rem" }}>
            Top 10 Scores
          </Typography>
          <Paper elevation={3}>
            <Table>
              <TableHead style={{ backgroundColor: "rgb(44, 139, 223)" }}>
                <TableRow>
                  <TableCell>ID</TableCell>
                  <TableCell align="center">Score</TableCell>
                  <TableCell align="center">Word</TableCell>
                </TableRow>
              </TableHead>
              <TableBody id="scores-table">
                {scores.map((row, index) => (
                  <TableRow key={index}>
                    <TableCell component="th" scope="row">
                      {index + 1}
                    </TableCell>
                    <TableCell align="center">{row.score}</TableCell>
                    <TableCell align="center">{row.word}</TableCell>
                  </TableRow>
                ))}
              </TableBody>
            </Table>
          </Paper>
        </Box>
      </Modal>
    </div>
  );
}

const style = {
  position: "absolute",
  top: "50%",
  left: "50%",
  transform: "translate(-50%, -50%)",
  width: "35%",
  bgcolor: "background.paper",
  border: "2px solid #000",
  boxShadow: 24,
  p: 4,
  textAlign: "center",
};

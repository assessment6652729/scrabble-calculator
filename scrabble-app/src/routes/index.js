import Container from "react-bootstrap/esm/Container";
import { Routes, Route } from "react-router";
import Scrabble from "../pages/scrabble";

function RouterProvider() {
  const routes = [
    {
      id: "r1",
      path: "/",
      component: Scrabble,
    },
  ];
  return (
    <Container>
      <Routes>
        {" "}
        {routes.map((route) => (
          <Route
            path={route.path}
            element={<route.component />}
            exact="{route.exact}"
            key={route.id}
          />
        ))}{" "}
      </Routes>
    </Container>
  );
}

export default RouterProvider;

import React from "react";
import ReactDOM from "react-dom/client";
import "./assets/css/index.css";
import "bootstrap/dist/css/bootstrap.min.css";
import Navbar from "./components/navbar";
import Routes from "./routes";
import { BrowserRouter as Router } from "react-router-dom";

const root = ReactDOM.createRoot(document.getElementById("root"));
root.render(
  <>
    <Navbar />
    <Router>
      <Routes />
    </Router>
  </>
);

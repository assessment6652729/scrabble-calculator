/**
 * Cypress E2E Testing
 */
describe("Scrabble Calculator", () => {
  beforeEach(() => {
    cy.visit("/");
  });

  it("Components rendered successfully", () => {
    cy.contains("Scrabble Points Calculator").should("be.visible");

    cy.get("#view-scores-btn").should("be.visible");

    cy.get("#save-score-btn").should("be.disabled");

    cy.get("#reset-tiles-btn").should("be.disabled");
  });

  it("Tiles rendered successfully", () => {
    cy.get(".tile-box").should("have.length", 10);
  });

  it("Fill tiles and button are enabled", () => {
    cy.get("#tile-0").type("A");
    cy.get("#tile-1").type("N");
    cy.get("#tile-2").type("D");

    cy.get("#save-score-btn").should("be.enabled");
    cy.get("#reset-tiles-btn").should("be.enabled");
  });

  it("Fill and Reset the tiles", () => {
    cy.get("#tile-0").type("A");
    cy.get("#tile-1").type("N");
    cy.get("#tile-2").type("D");

    cy.get("#total-score").should("have.text", "4");

    cy.get("#reset-tiles-btn").click();

    cy.get("#tile-0").should("have.value", "");
  });

  it("Fill and Save score", () => {
    cy.get("#tile-0").type("A");
    cy.get("#tile-1").type("N");
    cy.get("#tile-2").type("D");

    cy.intercept(
      {
        method: "POST",
        url: "api/*",
      },
      { fixture: "save-score-post.json" }
    ).as("score");

    cy.get("#save-score-btn").click();

    cy.wait(["@score"]);

    cy.contains("SAVED SCORE!").should("be.visible");
  });

  it("View top scores click", () => {
    cy.intercept(
      {
        method: "GET",
        url: "api/*",
      },
      { fixture: "top-scores-get.json" }
    ).as("scores");

    cy.get("#view-scores-btn").click();

    cy.wait(["@scores"]);

    cy.contains("Top 10 Scores").should("be.visible");

    cy.get("#scores-table").should("be.visible");
  });
});

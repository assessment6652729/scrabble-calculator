# Scrabble Score Calculator ReactJs Application

This simple calculator can assist in calculating each word's scrabble value for single-letter score. 

## Requirments

- Node js - v18.x

## Running the application

In the project directory `scrabble-app`, you can run below command to set up the project:

- `npm install`

To run the app: 

- `npm start`

Open [http://localhost:3000](http://localhost:3000) to view it in your browser.


For E2E testing, launches the test runner in the interactive watch mode. Trigger below command to run cypress:

- `npm test:cypress`
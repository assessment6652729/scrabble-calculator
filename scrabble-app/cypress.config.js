const { defineConfig } = require("cypress");

module.exports = defineConfig({
  chromeWebSecurity: false,
  video: false,
  screenshotsFolder: "reports/screenshots",
  videosFolder: "reports/videos",
  reporter: "mocha-junit-reporter",

  reporterOptions: {
    mochaFile: "reports/tests/cypress/junit-report-[hash].xml",
    testsuitesTitle: true,
  },

  component: {
    devServer: {
      framework: "create-react-app",
      bundler: "webpack",
    },
  },

  e2e: {
    setupNodeEvents(on, config) {
      // implement node event listeners here
    },
    baseUrl: "http://localhost:3000",
  },
});

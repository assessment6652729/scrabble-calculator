# Spring Boot Reactive RESTful API for Scrabble Calculator

Spring Boot WebFlux Rest API - an application that uses Spring Data Reactive (R2DBC) to interact with MySQL database.

## Requirements

- Maven
- Java 17
- MySQL

## Setting up database

To setup database schema and table, run the sql from 
- /src/main/resources/db_scripts.sql

## Running the application

In the project directory `scrabble-api`, you can run below command to run the application:

- `mvn spring-boot:run`

Application will start on port 9090. 

For HealthCheck 
- `http://localhost:9090/actuator/health`

For OpenAPI Documentation
- `http://localhost:9090/webjars/swagger-ui/index.html`
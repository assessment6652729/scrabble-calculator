package com.assessment.scrabble.api;

import com.assessment.scrabble.model.Score;
import com.assessment.scrabble.service.ScrabbleService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

class ScrabbleControllerTest {
    @Mock
    ScrabbleService scrabbleService;
    @InjectMocks
    ScrabbleController scrabbleController;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    void testSaveScore() {

        Mono<Score> mockScore = Mono.just(Score.builder().score(1).build());
        when(scrabbleService.saveScore(any())).thenReturn(mockScore);

        Mono<Score> result = scrabbleController.saveScore(Score.builder().score(1).build());
        result.subscribe(
                res -> assertEquals(1, res.getScore())
        );
    }

    @Test
    void testGetTopScoreUsers() {
        when(scrabbleService.findTopScoreList(anyInt())).thenReturn(Flux.empty());

        Flux<Score> result = scrabbleController.getTopScoreUsers(1);
        assertEquals(Flux.empty(), result);
    }
}

//Generated with love by TestMe :) Please report issues and submit feature requests at: http://weirddev.com/forum#!/testme
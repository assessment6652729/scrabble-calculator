package com.assessment.scrabble.service;

import com.assessment.scrabble.model.Score;
import com.assessment.scrabble.repository.ScoreRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.ArrayList;
import java.util.Collections;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

class ScrabbleServiceTest {
    @Mock
    ScoreRepository scoreRepository;
    @InjectMocks
    ScrabbleService scrabbleService;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    void testSaveScore() {
        Score score = Score.builder().score(10).build();
        Mono<Score> mockScore = Mono.just(score);
        when(scoreRepository.save(any())).thenReturn(mockScore);

        Mono<Score> result = scrabbleService.saveScore(score);
        result.subscribe(res -> assertEquals(score.getScore(), res.getScore()));
    }

    @Test
    void testGetTopScoreList() {
        ArrayList<Score> scoreList = new ArrayList<>(Collections.singletonList(
                Score.builder().build())
        );
        when(scoreRepository.findTopScoreList(anyInt())).thenReturn(Flux.fromIterable(scoreList));

        Flux<Score> result = scrabbleService.findTopScoreList( 2);
        result.collectList().subscribe(resultList -> {
            assertEquals(1, resultList.size());
        });
    }

}

//Generated with love by TestMe :) Please report issues and submit feature requests at: http://weirddev.com/forum#!/testme
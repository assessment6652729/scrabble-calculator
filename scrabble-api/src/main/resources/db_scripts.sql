CREATE SCHEMA `scrabble_app` ;

CREATE TABLE `scrabble_app`.`score` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `score` INT NOT NULL,
  `word` VARCHAR(10) NOT NULL,
  PRIMARY KEY (`id`));

package com.assessment.scrabble.dto;

public enum ErrorCode {
    SYSTEM_ERROR,
    VALIDATE_FAILED
}

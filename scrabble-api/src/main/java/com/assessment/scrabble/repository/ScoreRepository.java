package com.assessment.scrabble.repository;

import com.assessment.scrabble.model.Score;
import org.springframework.data.r2dbc.repository.Query;
import org.springframework.data.repository.reactive.ReactiveCrudRepository;
import reactor.core.publisher.Flux;

public interface ScoreRepository extends ReactiveCrudRepository<Score, Long> {

    Flux<Score> findFirst10ByOrderByScoreDesc();

    @Query("select distinct word, score from score order by score desc limit :count")
    Flux<Score> findTopScoreList(int count);

}

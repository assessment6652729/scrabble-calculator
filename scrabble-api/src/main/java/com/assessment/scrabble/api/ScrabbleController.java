package com.assessment.scrabble.api;

import com.assessment.scrabble.model.Score;
import com.assessment.scrabble.service.ScrabbleService;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.Valid;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Slf4j
@RestController
@RequestMapping("/api")
@Tag(name = "Scrabble Points Calculator APIs")
public class ScrabbleController {

    @Autowired
    private ScrabbleService scrabbleService;

    /**
     * Save Score API
     * @param score
     * @return
     */
    @PostMapping("/scores")
    public Mono<Score> saveScore(@Valid @RequestBody Score score){
        log.info("Save score - {}", score);
        return scrabbleService.saveScore(score);
    }

    /**
     * Get Top Score List API
     * @return
     */
    @GetMapping("/scores")
    public Flux<Score> getTopScoreUsers(@RequestParam(name = "count", required = false, defaultValue = "10") Integer count){
        log.info("Get Top Score Users : count - {}", count);
        return scrabbleService.findTopScoreList(count);
    }
}

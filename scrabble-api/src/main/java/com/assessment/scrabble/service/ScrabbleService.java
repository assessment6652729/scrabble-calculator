package com.assessment.scrabble.service;

import com.assessment.scrabble.model.Score;
import com.assessment.scrabble.repository.ScoreRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Service
public class ScrabbleService {

    @Autowired
    private ScoreRepository scoreRepository;

    /**
     * Save Score
     *
     * @param score
     * @return
     */
    public Mono<Score> saveScore(Score score) {
        return scoreRepository.save(score);
    }

    /**
     * Fetch top score list
     * not returning duplicate word and score
     *
     * @return
     */
    public Flux<Score> findTopScoreList(int count) {
        return scoreRepository.findTopScoreList(count);
    }

}

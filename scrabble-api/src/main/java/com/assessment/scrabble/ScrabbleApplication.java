package com.assessment.scrabble;

import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.info.Info;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.WebApplicationType;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.web.reactive.config.EnableWebFlux;

@OpenAPIDefinition(info = @Info(title = "APIs", version = "1.0", description = "Documentation APIs v1.0"))
@SpringBootApplication
@EnableWebFlux
public class ScrabbleApplication {

	public static void main(String[] args) {
		new SpringApplicationBuilder(ScrabbleApplication.class).web(WebApplicationType.REACTIVE).run(args);
	}

}

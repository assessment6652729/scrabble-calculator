package com.assessment.scrabble.handler;

import com.assessment.scrabble.dto.ErrorCode;
import com.assessment.scrabble.dto.ErrorResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.support.DefaultMessageSourceResolvable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.support.WebExchangeBindException;

import java.util.stream.Collectors;

@Slf4j
@ControllerAdvice
public class RequestHandler {

    /**
     * Generic Exception Handler
     * @param ex
     * @return
     */
    @ExceptionHandler(Exception.class)
    public final ResponseEntity<Object> handleAllExceptions(Exception ex){
        log.info("Inside Exception Handler - {}", ex.getLocalizedMessage());
        ErrorResponse errorResponse = ErrorResponse
                .builder()
                .resultCode(ErrorCode.SYSTEM_ERROR.name())
                .message(ex.getLocalizedMessage())
                .build();
        return ResponseEntity.internalServerError().body(errorResponse);
    }

    /**
     * Handle Request Validation Exception
     * @param ex
     * @return
     */
    @ExceptionHandler(WebExchangeBindException.class)
    public ResponseEntity<ErrorResponse> handleValidationExceptions(WebExchangeBindException ex){
        var errors = ex.getBindingResult()
                .getAllErrors()
                .stream()
                .map(DefaultMessageSourceResolvable::getDefaultMessage)
                .collect(Collectors.joining(","));
        log.info("Inside Validation Exception Handler - {}", errors);
        ErrorResponse errorResponse = ErrorResponse
                .builder()
                .resultCode(ErrorCode.VALIDATE_FAILED.name())
                .message(errors)
                .build();
        return ResponseEntity.badRequest().body(errorResponse);
    }
}

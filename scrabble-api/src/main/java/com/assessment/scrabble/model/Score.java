package com.assessment.scrabble.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.*;
import org.springframework.data.annotation.Id;
import org.springframework.data.relational.core.mapping.Column;
import org.springframework.data.relational.core.mapping.Table;

import java.io.Serializable;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table("score")
public class Score implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @Column("id")
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private Long scoreId;

    @Column
    private int score;

    @Column
    private String word;
}

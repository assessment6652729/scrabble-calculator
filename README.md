# Web App with React and Spring Boot

This project consists of two applications : scrabble-app, ReactJs application, and scrabble-api, Spring Boot Reactive RESTful API.
Users can use the web interface built with React to:
- calculate scrabble score in each word
- save score
- view top 10 scores

## Technologies used
- Java 17
- Spring Boot
- MySQL
- Node.js - v18.x
- React - React Router, Hooks

## Getting started

### Downloading
Click on the 'Code' button and clone this project via command line or select 'Download Zip.'

### Running ReactJs Application
1. Unzip the zip file if you have downloaded this project as a zip file.
2. Open the folder on the command line, such as Git Bash, Powershell or Terminal.
3. Run `npm install` in `scrabble-api` folder to install all dependencies to run the UI.
4. Run `npm start` to start the application.
5. Open your browser and visit [http://localhost:3000](http://localhost:3000).

### Running Spring Boot Application
1. In the `scrabble-api` folder, run db script from `./src/main/resources/db_scripts.sql` to create database schema and table
2. Run `mvn spring-boot:run` to start the application.
